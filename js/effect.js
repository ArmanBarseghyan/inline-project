// (function(){

//     var canvas = document.createElement('canvas'),
//     ctx = canvas.getContext('2d'),
//     w = canvas.width = innerWidth,
//     h = canvas.height = innerHeight,
//     particles = [],
//     properties = {
//         bgColor             : ' #fff',
//         particleColor       : '#0071C5',
//         particleRadius      : 3,
//         particleCount       : 30,
//         particleMaxVelocity : 0.5,
//         lineLength          : 150,
//         particleLife        : 6,
//     };

//     document.querySelector('.canvas').appendChild(canvas);

//     window.onresize = function(){
//         w = canvas.width = innerWidth,
//         h = canvas.height = innerHeight;        
//     }

//     class Particle{
//         constructor(){
//             this.x = Math.random()*w;
//             this.y = Math.random()*h;
//             this.velocityX = Math.random()*(properties.particleMaxVelocity*2)-properties.particleMaxVelocity;
//             this.velocityY = Math.random()*(properties.particleMaxVelocity*2)-properties.particleMaxVelocity;
//             this.life = Math.random()*properties.particleLife*60;
//         }
//         position(){
//             this.x + this.velocityX > w && this.velocityX > 0 || this.x + this.velocityX < 0 && this.velocityX < 0? this.velocityX*=-1 : this.velocityX;
//             this.y + this.velocityY > h && this.velocityY > 0 || this.y + this.velocityY < 0 && this.velocityY < 0? this.velocityY*=-1 : this.velocityY;
//             this.x += this.velocityX;
//             this.y += this.velocityY;
//         }
//         reDraw(){
//             ctx.beginPath();
//             ctx.arc(this.x, this.y, properties.particleRadius, 0, Math.PI*2);
//             ctx.closePath();
//             ctx.fillStyle = properties.particleColor;
//             ctx.fill();
//         }
//         reCalculateLife(){
//             if(this.life < 1){
//                 this.x = Math.random()*w;
//                 this.y = Math.random()*h;
//                 this.velocityX = Math.random()*(properties.particleMaxVelocity*2)-properties.particleMaxVelocity;
//                 this.velocityY = Math.random()*(properties.particleMaxVelocity*2)-properties.particleMaxVelocity;
//                 this.life = Math.random()*properties.particleLife*60;
//             }
//             this.life--;
//         }
//     }

//     function reDrawBackground(){
//         ctx.fillStyle = properties.bgColor;
//         ctx.fillRect(0, 0, w, h);
//     }

//     function drawLines(){
//         var x1, y1, x2, y2, length, opacity;
//         for(var i in particles){
//             for(var j in particles){
//                 x1 = particles[i].x;
//                 y1 = particles[i].y;
//                 x2 = particles[j].x;
//                 y2 = particles[j].y;
//                 length = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
//                 if(length < properties.lineLength){
//                     opacity = 1-length/properties.lineLength;
//                     ctx.lineWidth = '0.5';
//                     ctx.strokeStyle = 'rgb(43, 113, 197), '+opacity+')';
//                     ctx.beginPath();
//                     ctx.moveTo(x1, y1);
//                     ctx.lineTo(x2, y2);
//                     ctx.closePath();
//                     ctx.stroke();
//                 }
//             }
//         }
//     }

//     function reDrawParticles(){
//         for(var i in particles){
//             particles[i].reCalculateLife();
//             particles[i].position();
//             particles[i].reDraw();
//         }
//     }

//     function loop(){
//         reDrawBackground();
//         reDrawParticles();
//         drawLines();
//         requestAnimationFrame(loop);
//     }

//     function init(){
//         for(var i = 0 ; i < properties.particleCount ; i++){
//             particles.push(new Particle);
//         }
//         loop();
//     }

//     init();

// }())



// aaaaaaaaa



// (() => {
//         const config = {
//           dotMinRad  : 4,
//           dotMaxRad  : 20,
//           sphereRad  : 30,
//           bigDotRad  : 1,
//           mouseSize  : 120,
//           massFactor : 0.001,
//           defColor   : `#0071C5`,
//           smooth     : 0.85,
//         }
      
//         const TWO_PI = 2 * Math.PI;
//         const canvas = document.querySelector(`canvas`);
//         const ctx    = canvas.getContext(`2d`);
      
//         let w, h, mouse, dots;
      
//         class Dot {
//           constructor(r) {
//             this.pos   = {x: mouse.x, y: mouse.y}
//             this.vel   = {x: 0, y: 0}
//             this.rad   = r || random(config.dotMinRad, config.dotMaxRad);
//             this.mass  = this.rad * config.massFactor;
//             this.color = config.defColor;
//           }
      
//           draw(x, y) {
//             this.pos.x = x || this.pos.x + this.vel.x;
//             this.pos.y = y || this.pos.y + this.vel.y;
//             createCircle(this.pos.x, this.pos.y, this.rad, true, this.color);
//             createCircle(this.pos.x, this.pos.y, this.rad, false, config.defColor);
//           }
//         }
      
//         function updateDots() {
//           for (let i = 1; i < dots.length; i++) {
//             let acc = {x: 0, y: 0}
      
//             for (let j = 0; j < dots.length; j++) {
//               if (i == j) continue;
//               let [a, b] = [dots[i], dots[j]];
      
//               let delta  = {x: b.pos.x - a.pos.x, y: b.pos.y - a.pos.y}
//               let dist   = Math.sqrt( delta.x * delta.x + delta.y * delta.y) || 1;
//               let force  = (dist - config.sphereRad) / dist * b.mass;
      
//               if (j == 0) {
//                 let alpha = config.mouseSize / dist;
//                 a.color   = `#0071C5, ${alpha})`;
      
//                 dist < config.mouseSize ? force = (dist - config.mouseSize) * b.mass : force = a.mass;
//               }
      
//               acc.x     += delta.x * force;
//               acc.y     += delta.y * force;
//             }
      
//             dots[i].vel.x = dots[i].vel.x * config.smooth + acc.x * dots[i].mass;
//             dots[i].vel.y = dots[i].vel.y * config.smooth + acc.y * dots[i].mass;
//           }
          
//           dots.map(e => e == dots[0] ? e.draw(mouse.x, mouse.y) : e.draw());
//         }
      
//         function createCircle(x, y, rad, fill, color) {
//           ctx.fillStyle = ctx.strokeStyle = color;
//           ctx.beginPath();
//           ctx.arc(x, y, rad, 0, TWO_PI);
//           ctx.closePath();
//           fill ? ctx.fill() : ctx.stroke();
//         }
      
//         function random(min, max) {
//           return Math.random() * (max - min) + min;
//         }
      
//         function init() {
//           w     = canvas.width  = innerWidth;
//           h     = canvas.height = innerHeight;
      
//           mouse = {x: w / 2, y: h / 2, down: false}
//           dots  = [];
      
//           dots.push(new Dot(config.bigDotRad));
//         }
      
//         function loop() {
//           ctx.clearRect(0, 0, w, h);
      
//           if (mouse.down) { dots.push(new Dot()); }
//           updateDots();
      
//           window.requestAnimationFrame(loop);
//         }
      
//         init();
//         loop();
      
//         function setPos({layerX, layerY}) {
//           [mouse.x, mouse.y] = [layerX, layerY];
//         }
      
//         function isDown() {
//           mouse.down = !mouse.down;
//         }
      
//         canvas.addEventListener(`mousemove`, setPos);
//         window.addEventListener(`mousedown`, isDown);
//         window.addEventListener(`mouseup`  , isDown);
//       })();