
let cubeOne = document.getElementById('cubeOne');
let cubeTwo = document.getElementById('cubeTwo');

window.addEventListener('scroll', function () {
    let value = window.scrollY;
    if (cubeOne) {
        cubeOne.style.marginTop = value * 2.4 + 'px';
        cubeOne.style.marginLeft = value * 0.2 + 'px';
    }
    if (cubeTwo) {
        cubeTwo.style.marginTop = value * 1.9 + 'px';
        cubeTwo.style.marginLeft = value * 1.3 + 'px';
    }
    if (cubeThree) {
        cubeThree.style.marginTop = value * 1.9 + 'px';
        cubeThree.style.marginLeft = value * 0.3 + 'px';
    }
    if (cubeFour) {
        cubeFour.style.marginTop = value * 2.5 + 'px';
        cubeFour.style.marginLeft = value * 1.2 + 'px';
    }
    if (cubeFive) {
        cubeFive.style.marginTop = value * 1.6 + 'px';
        cubeFive.style.marginLeft = value * -1 + 'px';
    }
    if (cubeSix) {
        cubeSix.style.marginTop = value * 2.9 + 'px';
        cubeSix.style.marginLeft = value * -1.5 + 'px';
    }    
})
