var swiper = new Swiper(".OtherProductSwiper", {
    slidesPerView: 2,
    // centeredSlides: true,
    spaceBetween: 8,
    
    breakpoints: {
      480: {
        spaceBetween: 30,
      }
     },
     pagination: {
      el: ".swiper-pagination",
      type: "fraction",
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });

