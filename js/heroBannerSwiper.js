var swiper = new Swiper(".HeroBannerSwiper", {
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    autoplay: {
      delay: 5000,
  },
  });



  var swiper = new Swiper(".EquipmentSwiper", {
    slidesPerView: 3,
    spaceBetween: 12,
    breakpoints: {
        767: {
          spaceBetween: 16,
        }
       },
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
  });