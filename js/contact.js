// document.getElementsByClassName('dropdown')[0].onclick = function() {
//     document.getElementsByClassName('dropdown')[0].classList.add('dropdown-active');
//     document.getElementsByClassName('burger-control')[0].classList.remove('burger-open');
// }

// document.getElementsByClassName('dropdown')[0].onclick = function() {
//     document.getElementsByClassName('dropdown')[0].classList.add('burger-open');
//     document.getElementsByClassName('dropdown')[0].classList.remove('burger-hidden');
// }

let dropdown = document.querySelector('.dropdown');

dropdown.onclick = function() {
    if(dropdown.classList.contains('dropdown-active')){
        dropdown.classList.remove('dropdown-active');
    } else{
        dropdown.classList.add('dropdown-active')
    }
}



$(document).on('click', '.dropdown-control .dropdown-item', function() {
    $('.dropdown-control .dropdown-item').removeClass('selected');
    $(this).addClass('selected');

    let selectedText = $(this).text();
    $(this).parent().siblings('.select-text').text(selectedText);
});


$('form#contact_form').on('submit', function(e) {
    e.preventDefault();
    let form = $(this);

    var formData = {
        SendMessageText: form.find('textarea[name="message-text"]').val(),
        SenderEmail: form.find('input[name="sender-email"]').val(),
        SenderName: form.find('input[name="sender-name"]').val(),
        SenderPhone: form.find('input[name="sender-phone"]').val(),
        CompanyName: form.find('input[name="company-name"]').val(),
        CompanyIndustry: form.find('.dropdown-item.selected').text()
    }

    $.ajax({
        url: "https://inlinephotonics.innline.am/api/callback/send",
        type: "POST",
        crossDomain: true,
        data: JSON.stringify(formData),
        contentType: "application/json; charset=utf-8",

        success: function (response) {
            form.fadeOut(function() {
                form.siblings('.form-sent').fadeIn();
            })
        },
        error: function (xhr, status) {
            console.log(xhr);
        }
    });
});

$(document).on('click', '.form-sent .resend', function() {

    $(this).parent('.form-sent').fadeOut(function() {

        $("form#contact_form").fadeIn();

    })

});


function onloadCallback() {
    var captcha = grecaptcha.getResponse();
    /* Place your recaptcha rendering code here */
}
// // Работа с виджетом recaptcha
// // 1. Получить ответ гугл капчи

// // 2. Если ответ пустой, то выводим сообщение о том, что пользователь не прошёл тест.
// // Такую форму не будем отправлять на сервер.
// if (!captcha.length) {
//   // Выводим сообщение об ошибке
//   $('#recaptchaError').text('* Вы не прошли проверку "Я не робот"');
// } else {
//   // получаем элемент, содержащий капчу
//   $('#recaptchaError').text('');
// }

// // 3. Если форма валидна и длина капчи не равно пустой строке, то отправляем форму на сервер (AJAX)
// if ((formValid) && (captcha.length)) {
//   console.log("valid");  
// }

// // 4. Если сервер вернул ответ error, то делаем следующее...
// // Сбрасываем виджет reCaptcha
// grecaptcha.reset();
// // Если существует свойство msg у объекта $data, то...
// if ($data.msg) {
//   // вывести её в элемент у которого id=recaptchaError
//   $('#recaptchaError').text($data.msg);
// }